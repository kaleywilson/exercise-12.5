
public class NumberDisplayImpl implements NumberDisplay {
	private SevenSegment display;
	
	public NumberDisplayImpl(SevenSegment display) {
		this.display = display;
	}
	@Override
	public void display(int number) {
		for(int i = 1; i <= 7; i++) {
			display.setLED(i, false);
		}
		switch(number) {
		case 0:
			display.setLED(1, true);
			display.setLED(2, true);
			display.setLED(3, true);
			display.setLED(5, true);
			display.setLED(6, true);
			display.setLED(7, true);
			break;
		case 1:
			display.setLED(3, true);
			display.setLED(6, true);
			break;
		case 2: 
			display.setLED(1, true);
			display.setLED(3, true);
			display.setLED(4, true);
			display.setLED(5, true);
			display.setLED(7, true);
			break;
		case 3:
			display.setLED(1, true);
			display.setLED(3, true);
			display.setLED(4, true);
			display.setLED(6, true);
			display.setLED(7, true);
			break;
		case 4:
			display.setLED(2, true);
			display.setLED(3, true);
			display.setLED(4, true);
			display.setLED(6, true);
			break;
		case 5:
			display.setLED(1, true);
			display.setLED(2, true);
			display.setLED(4, true);
			display.setLED(6, true);
			display.setLED(7, true);
			break;
		case 6:
			display.setLED(1, true);
			display.setLED(2, true);
			display.setLED(4, true);
			display.setLED(5, true);
			display.setLED(6, true);
			display.setLED(7, true);
			break;
		case 7:
			display.setLED(1, true);
			display.setLED(3, true);
			display.setLED(6, true);
			break;
		case 8:
			for(int i = 1; i <= 7; i++) {
				display.setLED(i, true);
			}
			break;
		case 9:
			display.setLED(1, true);
			display.setLED(2, true);
			display.setLED(3, true);
			display.setLED(4, true);
			display.setLED(6, true);
			display.setLED(7, true);
			break;
		}
		
	}
	public boolean getLed(int led) {
		return display.getLED(led);
	}

}
