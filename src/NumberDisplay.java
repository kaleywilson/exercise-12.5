/** The interface to a seven segment LED that displays numbers 0-9.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/

public interface NumberDisplay {
  /** display a number on a seven segment.
   * @param number the number to display.  
   * Precondition: number should be in the range 0 to 9.
  */
  void display(int number); 
}
