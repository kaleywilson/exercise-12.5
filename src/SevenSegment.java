/** Defines the contract for a seven segment LED display.
 
   This source code is from the book 
     "Flexible, Reliable Software:
       Using Patterns and Agile Development"
     published 2010 by CRC Press.
   Author: 
     Henrik B Christensen 
     Computer Science Department
     Aarhus University
   
   This source code is provided WITHOUT ANY WARRANTY either 
   expressed or implied. You may study, use, modify, and 
   distribute it for non-commercial purposes. For any 
   commercial use, see http://www.baerbak.com/
*/

public interface SevenSegment {
  /** turn a LED on or off.
   * @param led the number of the LED. Range is 0 to 6. The LEDs are
   * numbered top to bottom, left to right. That is, the top,
   * horizontal, LED is 0, the top left LED is 1, etc.
   * @param on if true the LED is turned on otherwise it is turned
   * off.
  */
  void setLED(int led, boolean on);
}
