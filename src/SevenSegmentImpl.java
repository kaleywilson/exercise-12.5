
public class SevenSegmentImpl implements SevenSegment {
	boolean one, two, three, four, five, six, seven;
	public SevenSegmentImpl() {
		one = false;
		two = false;
		three = false;
		four = false;
		five = false;
		six = false;
		seven = false;
	}

	@Override
	public void setLED(int led, boolean on) {
		switch(led) {
		case 1: 
			one = on;
			break;
		case 2:
			two = on;
			break;
		case 3:
			three = on;
			break;
		case 4: 
			four = on;
			break;
		case 5:
			five = on;
			break;
		case 6:
			six = on;
			break;
		case 7:
			seven = on;
			break;
		default:
			break;
		}
		
	}
	
	public boolean getLED(int led) {
		switch(led) {
		case 1: 
			return one;
		case 2:
			return two;
		case 3:
			return three;
		case 4: 
			return four;
		case 5:
			return five;
		case 6:
			return six;
		case 7:
			return seven;
		default:
			return false;
		}
	}
}
