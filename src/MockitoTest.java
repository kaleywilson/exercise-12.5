import org.mockito.*;
import org.mockito.junit.*;
import static org.mockito.Mockito.*;
import org.junit.*;
import static org.junit.Assert.*;

public class MockitoTest {
	private SevenSegment segment;
	private NumberDisplay display;
	
	public MockitoTest() {
		segment = Mockito.mock(SevenSegment.class);
		display = new NumberDisplayImpl(segment);
	}
	
	@Test
	public void test() {
		display.display(0);
		verify(segment).setLED(1, true);
		verify(segment).setLED(2, true);
		verify(segment).setLED(3, true);
		verify(segment).setLED(4, false);
		verify(segment).setLED(5, true);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, true);
	}
	@Test
	public void test1() {
		display.display(1);
		verify(segment).setLED(1, false);
		verify(segment).setLED(2, false);
		verify(segment).setLED(3, true);
		verify(segment).setLED(4, false);
		verify(segment).setLED(5, false);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, false);
	}
	
	@Test
	public void test2(){
		display.display(2);
		verify(segment).setLED(1, true);
		verify(segment).setLED(2, false);
		verify(segment).setLED(3, true);
		verify(segment).setLED(4, true);
		verify(segment).setLED(5, true);
		verify(segment).setLED(6, false);
		verify(segment).setLED(7, true);
	}
	
	@Test
	public void test3(){
		display.display(3);
		verify(segment).setLED(1, true);
		verify(segment).setLED(2, false);
		verify(segment).setLED(3, true);
		verify(segment).setLED(4, true);
		verify(segment).setLED(5, false);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, true);
	}
	@Test
	public void test4(){
		display.display(4);
		verify(segment).setLED(1, false);
		verify(segment).setLED(2, true);
		verify(segment).setLED(3, true);
		verify(segment).setLED(4, true);
		verify(segment).setLED(5, false);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, false);
	}
	@Test
	public void test5(){
		display.display(5);
		verify(segment).setLED(1, true);
		verify(segment).setLED(2, true);
		verify(segment).setLED(3, false);
		verify(segment).setLED(4, true);
		verify(segment).setLED(5, false);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, true);
	}
	@Test
	public void test6(){
		display.display(6);
		verify(segment).setLED(1, true);
		verify(segment).setLED(2, true);
		verify(segment).setLED(3, false);
		verify(segment).setLED(4, true);
		verify(segment).setLED(5, true);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, true);
	}
	@Test
	public void test7(){
		display.display(7);
		verify(segment).setLED(1, true);
		verify(segment).setLED(2, false);
		verify(segment).setLED(3, true);
		verify(segment).setLED(4, false);
		verify(segment).setLED(5, false);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, false);
	}
	@Test
	public void test8(){
		display.display(8);
		verify(segment).setLED(1, true);
		verify(segment).setLED(2, true);
		verify(segment).setLED(3, true);
		verify(segment).setLED(4, true);
		verify(segment).setLED(5, true);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, true);
	}
	@Test
	public void test9(){
		display.display(9);
		verify(segment).setLED(1, true);
		verify(segment).setLED(2, true);
		verify(segment).setLED(3, true);
		verify(segment).setLED(4, true);
		verify(segment).setLED(5, false);
		verify(segment).setLED(6, true);
		verify(segment).setLED(7, true);	
	}
}
