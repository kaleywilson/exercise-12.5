import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class NumberDisplayTester {
	private NumberDisplayImpl display;
	public NumberDisplayTester() {
		display = new NumberDisplayImpl();
	}
	@Test
	public void test() {
		display.display(0);
		assertEquals(display.getLed(1), true);
		assertEquals(display.getLed(2), true);
		assertEquals(display.getLed(3), true);
		assertEquals(display.getLed(4), false);
		assertEquals(display.getLed(5), true);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), true);
		display.display(1);
		assertEquals(display.getLed(1), false);
		assertEquals(display.getLed(2), false);
		assertEquals(display.getLed(3), true);
		assertEquals(display.getLed(4), false);
		assertEquals(display.getLed(5), false);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), false);
		display.display(2);
		assertEquals(display.getLed(1), true);
		assertEquals(display.getLed(2), false);
		assertEquals(display.getLed(3), true);
		assertEquals(display.getLed(4), true);
		assertEquals(display.getLed(5), true);
		assertEquals(display.getLed(6), false);
		assertEquals(display.getLed(7), true);
		display.display(3);
		assertEquals(display.getLed(1), true);
		assertEquals(display.getLed(2), false);
		assertEquals(display.getLed(3), true);
		assertEquals(display.getLed(4), true);
		assertEquals(display.getLed(5), false);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), true);
		display.display(4);
		assertEquals(display.getLed(1), false);
		assertEquals(display.getLed(2), true);
		assertEquals(display.getLed(3), true);
		assertEquals(display.getLed(4), true);
		assertEquals(display.getLed(5), false);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), false);
		display.display(5);
		assertEquals(display.getLed(1), true);
		assertEquals(display.getLed(2), true);
		assertEquals(display.getLed(3), false);
		assertEquals(display.getLed(4), true);
		assertEquals(display.getLed(5), false);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), true);
		display.display(6);
		assertEquals(display.getLed(1), true);
		assertEquals(display.getLed(2), true);
		assertEquals(display.getLed(3), false);
		assertEquals(display.getLed(4), true);
		assertEquals(display.getLed(5), true);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), true);
		display.display(7);
		assertEquals(display.getLed(1), true);
		assertEquals(display.getLed(2), false);
		assertEquals(display.getLed(3), true);
		assertEquals(display.getLed(4), false);
		assertEquals(display.getLed(5), false);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), false);
		display.display(8);
		assertEquals(display.getLed(1), true);
		assertEquals(display.getLed(2), true);
		assertEquals(display.getLed(3), true);
		assertEquals(display.getLed(4), true);
		assertEquals(display.getLed(5), true);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), true);
		display.display(9);
		assertEquals(display.getLed(1), true);
		assertEquals(display.getLed(2), true);
		assertEquals(display.getLed(3), true);
		assertEquals(display.getLed(4), true);
		assertEquals(display.getLed(5), false);
		assertEquals(display.getLed(6), true);
		assertEquals(display.getLed(7), true);

		
	}

}
